import React from 'react';

const Form = props =>{
    return(
        //kontrolowany komponent
        <form>     
            <input 
            type="text" 
            value={props.value}
            onChange={props.change}
            placeholder="Wpisz miasto"
            /> 
        </form>
        //<button>Wyszukaj miasta</button>
    )
}

export default Form