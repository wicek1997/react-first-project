import React, { Component } from 'react';
import Form from './Form';
import Result from './Result';
import './App.css';

const APIKey = '402b4dea1b1dc1161cd9bc3dbf11a4c8'

class App extends Component {

  //miejsca w ktorych przechowywane sa dane
  state = {
    value: '',
    date: '',
    city: '',
    sunrise: '',
    sunset: '',
    temp: '',
    pressure:'',
    wind: '',
    err: false,
  }

  //metoda do ubslugi zmiany inputa
  handleInputChange = e => {
    //zmiana stanu setState()
    this.setState({
      value: e.target.value
    })
  }
/*
  //zeby nie odswiezalo sie za kazdym razem kiedy szukamy miasta
  handleCitySubmit = e => {
    e.preventDefault()
    console.log("Potwierdzono formularz")
    const API =
    `https://api.openweathermap.org/data/2.5/weather?q=${this.state.value}&appid=${APIKey}`;

    fetch(API)
     //jesli spelniony
    .then(response =>{ 
      if(response.ok){
        return response
      }
      throw Error("Cos sie popsulo")

    })   
    .then(response => response.json())
    .then(data => {
      const time = new Date().toLocaleString()
      this.setState(state => ({
        err: false,
        date: time,
        sunrise: data.sys.sunrise,
        sunset: data.sys.sunset,
        temp: data.main.temp,
        pressure: data.main.pressure,
        wind: data.wind.speed,
        city: state.value,
      }))
    })
    .catch(err => {
      console.log(err);
      this.setState(prevState=>({
          err: true ,
          city: prevState.value 
        }))
    })  //jesli odrzucony

  }
*/
//cykl zycia komponentu
componentDidUpdate(prevProps,prevState){
  if (this.state.value.length == 0) return
  if (prevState.value !== this.state.value){
    const API =
    `https://api.openweathermap.org/data/2.5/weather?q=${this.state.value}&appid=${APIKey}`;

    fetch(API)
     //jesli spelniony
    .then(response =>{ 
      if(response.ok){
        return response
      }
      throw Error("Cos sie popsulo")

    })   
    .then(response => response.json())
    .then(data => {
      const time = new Date().toLocaleString()
      this.setState(state => ({
        err: false,
        date: time,
        sunrise: data.sys.sunrise,
        sunset: data.sys.sunset,
        temp: data.main.temp,
        pressure: data.main.pressure,
        wind: data.wind.speed,
        city: state.value,
      }))
    })
    .catch(err => {
      console.log(err);
      this.setState(prevState=>({
          err: true ,
          city: prevState.value 
        }))
    })  //jesli odrzucony
  }
}//po kazdej zmianie mozna go wywolac

  render(){
    return (
      <div className="App">
        <Form 
        value={this.state.value} 
        change={this.handleInputChange}
        />
        <Result weather={this.state} />
      </div>
    );
  }
}

export default App;
